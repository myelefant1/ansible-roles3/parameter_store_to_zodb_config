:warning: this role is deprecated and you are encouraged to use (parameter_store_to_config)[https://gitlab.com/myelefant1/ansible-roles3/parameter_store_to_config]

# Parameter store to zodb config

This role:
  * installs awscli and jq
  * installs service parameter_store_to_zodb_config to generate a file zodb.config

## Role parameters

| name                    | value       | default_value   | description                                 |
| ------------------------|-------------|-----------------|---------------------------------------------|
| parameter_store_to_zodb_config_service_state           | string      | started         | State of the systemd service after the run               |
| parameter_store_to_zodb_config_aws_region              | string      | false           | AWS region where ParameterStore is configured            |
| parameter_store_to_zodb_config_parameter_path          | string      | false           | Path for settings in ParameterStore                      |
| parameter_store_to_zodb_config_parameter_store_output  | string      | false           | Location of generated zodb_config file                           |


## Using this role

### ansible galaxy
Add the following in your *requirements.yml*.
```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/parameter_store_to_zodb_config.git
  scm: git
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: parameter-store-to-zodb_config
      parameter_store_to_zodb_config_aws_region: us-east-1
      parameter_store_to_zodb_config_parameter_path: /production
      parameter_store_to_zodb_config_parameter_store_output: /tmp/zodb.config
```

